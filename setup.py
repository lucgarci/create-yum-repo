#!/usr/bin/env python

from distutils.core import setup

setup(name='create-yum-repo',
      version='0.1',
      description='Git-Ops based compose tool',
      scripts=['create-yum-repo.py', 'test-yum-repo.py', 'preview.py', 'list-yum-repo.py'],
      data_files=[('bin', ['dnf.conf.j2', 'yumrepo.j2', 'package_build_dict.json'])],
     )

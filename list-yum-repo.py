#!/usr/bin/env python3

import logging
import logging.config
import os
import sys
import json
import tempfile
from concurrent import futures
from pathlib import Path

import koji
import shutil
import boto3
import requests
from jinja2 import Template
from requests.adapters import HTTPAdapter
import datetime
import subprocess

def run_cmd(cmd: list) -> int:
    cp = subprocess.run(
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    if cp.returncode != 0:
        logging.error(
            f"Command {cmd} failed exit code: {cp.returncode}\n"
            f"Command {cmd} args: {cp.args}\n"
            f"Command {cmd} stdout: {cp.stdout}\n"
            f"Command {cmd} stderr: {cp.stderr}\n"
        )
        return 1
    else:
        logging.info(
            "Successfully run the command {}\n"
            "Command {} stdout: {}\n".format(cmd, cmd, cp.stdout.decode("utf-8"))
        )

    return 0

def list_repo(baseurl: str, arch: str) -> int:
    """Call dnf makecache to test the created repos are valid"""
    logging.info(f"Testing dnf repo at {baseurl} for {arch}")

    # Configure and call dnf using a custom dnf.conf to avoid contaminating the host system
    dnfconf_template_file = Path(__file__).parent / "dnf.conf.j2"
    repofile_template_file = Path(__file__).parent / "yumrepo.j2"

    with tempfile.TemporaryDirectory() as tmpdirname:
        tmpdir = Path(tmpdirname)
        (tmpdir / "cache").mkdir(parents=True)
        (tmpdir / "logs").mkdir(parents=True)
        (tmpdir / "persist").mkdir(parents=True)
        (tmpdir / "repos").mkdir(parents=True)

        dnfconf = Template(dnfconf_template_file.read_text()).render(tmpdir=tmpdirname)
        (tmpdir / "dnf.conf").write_text(dnfconf)

        repofile = Template(repofile_template_file.read_text()).render(
            arch=arch, baseurl=baseurl
        )
        (tmpdir / "repos" / "automotive.repo").write_text(repofile)
        logging.info(repofile)

        result = run_cmd(
            cmd=[
                "dnf",
                "-v",
                "-c",
                f"{tmpdirname}/dnf.conf",
                "list",
                "--repo=auto",
            ]
        )
        return result

def list_remote_repo(
    base_url: str, arch: str
) -> int:
    """Call dnf list to show details of the uploaded repos"""
    return list_repo(base_url, arch)

def main() -> int:
    logging.getLogger().setLevel(logging.INFO)
    logging.info(":: " + sys.argv[0] + " started ::")
    logging.basicConfig(stream=sys.stdout)

    supported_arches = ["aarch64", "x86_64"]

    base_url = sys.argv[1]
    logging.info(
        "Testing repos at: %s", base_url
    )
    # Test all version/arch combos for this version
    for arch in supported_arches:
        returnCode = list_remote_repo(
            base_url=base_url,
            arch=arch,
        )
        if returnCode:
            return 1

    return 0


if __name__ == "__main__":
    sys.exit(main())

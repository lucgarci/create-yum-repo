.PHONY: dependencies unit

dependencies:
	pip install -r tests/requirements.txt


unit: dependencies
	PYTHONPATH="$(PYTHONPATH):../create_yum_repo" pytest -v tests/unit

import boto3
import os
import pathlib
import py
import pytest

from jinja2 import Template
from moto import mock_s3


@pytest.fixture
def upload_repodir(tmpdir: py.path.local) -> str:
    dir_path = tmpdir.mkdir("/var").mkdir("/lib").mkdir("/repos")
    dir_path.mkdir("cs8")
    dir_path.mkdir("cs9")
    (dir_path / "cs8" / "test1.rpm").write("hello")
    (dir_path / "cs9" / "test2.rpm").write("hello")
    return str(dir_path)


@pytest.fixture
def download_repodir(tmpdir: py.path.local) -> str:
    dir_path = tmpdir.mkdir("/var").mkdir("/lib").mkdir("/repos")
    return str(dir_path)


@pytest.fixture
def tmp_repo_dir(tmpdir: py.path.local) -> str:
    return str(tmpdir.mkdir("tmp").mkdir("yum.repos.d"))


@pytest.fixture
def dnf_conf(tmpdir: py.path.local, tmp_repo_dir: str) -> str:
    reposdir = tmp_repo_dir
    cache_dir = tmpdir.mkdir("/tmp/cache")
    persistdir = tmpdir.mkdir("/tmp/persistdir")
    logdir = tmpdir.mkdir("/tmp/logdir")

    config_definition = [
        "[main]\n",
        "gpgcheck=0\n",
        "installonly_limit=3\n",
        "clean_requirements_on_remove=True\n",
        "best=True\n",
        "skip_if_unavailable=False\n",
        f"cachedir={cache_dir}\n",
        f"reposdir={reposdir}\n",
        f"persistdir={persistdir}\n",
        f"logdir={logdir}\n",
        "plugins=false",
    ]

    dnf_conf_path = tmpdir.join("./dnf.conf")

    with open(dnf_conf_path, "w") as config_file:
        config_file.writelines(config_definition)

    return str(dnf_conf_path)


@pytest.fixture
def repo(tmp_repo_dir: str) -> str:
    repo_definition = [
        "[examplerepo]\n",
        "name=Example Repository\n",
        "baseurl=https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/AppStream/x86_64/os/\n",
        "enabled=1\n",
        "gpgcheck=1\n",
    ]

    with open(f"{tmp_repo_dir}/examplerepo.repo", "w") as repofile:
        repofile.writelines(repo_definition)

    return f"{tmp_repo_dir}/examplerepo.repo"


@pytest.fixture
def s3_client() -> boto3.client:
    with mock_s3():
        conn = boto3.client("s3", region_name="us-east-1")
        yield conn


@pytest.fixture
def s3_resource() -> boto3.resources:
    with mock_s3():
        s3 = boto3.resource("s3")
        yield s3


@pytest.fixture
def bucket(s3_client: boto3.client) -> str:
    s3_client.create_bucket(Bucket="bucket")
    return s3_client.list_buckets()["Buckets"][0]["Name"]


@pytest.fixture
def boto3_session() -> boto3.session:
    return boto3.Session(region_name="us-east-1")


@pytest.fixture
def file(repodir: str) -> pathlib.Path:
    with open(f"{repodir}/myfile.txt", "w") as file:
        file.write("dummy")
    return pathlib.Path(f"{repodir}/myfile.txt")


@pytest.fixture
def repositories() -> list:
    return ["auto"]


@pytest.fixture
def supported_arches() -> str:
    return ["aarch64", "x86_64"]


@pytest.fixture(params=["cs9", "none"])
def mirror_list(request) -> list:
    mirrors = {
        "cs9": [
            f"http://mirror.stream.centos.org/9-stream/",
            f"https://composes.stream.centos.org/development/latest-CentOS-Stream/compose/",
        ],
        "none": [],
    }
    return mirrors[request.param]

@pytest.fixture
def koji_mirrors() -> list:
    return [
        "https://kojihub.stream.centos.org/kojifiles/packages/|https://kojihub.stream.centos.org/kojihub",
    ]

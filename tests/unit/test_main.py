import boto3
import json
import koji
import logging
import os
import pathlib
import pytest
import sys
import unittest

from botocore.exceptions import ClientError

import importlib
main = importlib.import_module("create-yum-repo")


def test_createrepo_succeeded(download_repodir: str) -> bool:
    assert main.run_cmd(["createrepo_c", download_repodir]) == 0

def test_createrepo_failed() -> bool:
    assert main.run_cmd(["createrepo_c", "/no/repo/"]) == 1

def test_repo_succeeded(dnf_conf: str, repo: str) -> bool:
    assert main.run_cmd(["dnf", "-v", "-c", f"{dnf_conf}", "makecache", "--repo=examplerepo"]) == 0

def test_repo_failed(dnf_conf: str, repo: str) -> bool:
    assert main.run_cmd(["dnf", "-v", "-c", f"{dnf_conf}", "makecache", "--repo=unknownrepo"]) == 1

def test_upload_directory(
    upload_repodir: str, bucket: str, boto3_session: str, s3_client: boto3.client
) -> bool:
    assert main.upload_directory(upload_repodir, "pipeline-run", bucket) == 0
    response = s3_client.list_objects_v2(Bucket=bucket)
    files_in_bucket = []
    for content in response["Contents"]:
        files_in_bucket.append(content["Key"])

    assert "pipeline-run/cs8/test1.rpm" in files_in_bucket
    assert "pipeline-run/cs9/test2.rpm" in files_in_bucket

def test_download_packages(
    download_repodir: str,
    supported_arches: list,
    repositories: list,
    mirror_list: list,
    koji_mirrors: list,
) -> bool:

    def fakeGetRPM(nvr):
        with open("./tests/resources/getRPM.json", "r") as f:
            fake_data = json.load(f)
        return fake_data[nvr]

    def fakeGetBuild(build_id):
        with open("./tests/resources/getBuild.json", "r") as f:
            fake_data = json.load(f)
        return fake_data[str(build_id)]

    #with unittest.mock.patch("koji.ClientSession") as mock_session,
    with unittest.mock.patch("koji.ClientSession.getRPM", create=True) as mockGetRPM,\
         unittest.mock.patch("koji.ClientSession.getBuild", create=True) as mockGetBuild:

        mockGetRPM.side_effect = fakeGetRPM
        mockGetBuild.side_effect = fakeGetBuild

        # Repeat download loop three times to ensure directory cleanup works
        package_list_dir = "./tests/resources/manifest_repo/package_list"
        for i in range(3):
            for supported_arch in supported_arches:
                assert main.download_packages(
                    package_list_dir,
                    "cs9",
                    download_repodir,
                    supported_arch,
                    repositories,
                    mirror_list,
                    koji_mirrors,
                ) == 0

                assert os.path.exists(f"{download_repodir}/{supported_arch}/os")
                assert os.path.exists(f"{download_repodir}/{supported_arch}/os/Packages")

            # Magic number comes from packages in tests/resources/manifest_repo/package_list
            rpms = list((pathlib.Path(download_repodir)).rglob("*.rpm"))
            print(rpms)
            assert len(rpms) == 8
